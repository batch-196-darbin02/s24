/*
	Activity:

	1. Update and Debug the following codes to ES6.
		Use template literals,
		Use array/object destructuring,
		Use arrow functions
	
    2. Create a class constructor able to receive 3 arguments
        It should be able to receive 3 strings and a number
        Using the this keyword assign properties:
        username, 
        role, 
        guildName,
        level 
  assign the parameters as values to each property.
  Create 2 new objects using our class constructor.
  This constructor should be able to create Character objects.



	Create 2 new objects using our class constructor.

	This constructor should be able to create Dog objects.

	Log the 2 new Dog objects in the console.


	Pushing Instructions:

	Create a git repository named S24.
	Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	Add the link in Boodle.
*/
// Solution: 

/* Debug */
let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101","Social Sciences 201"]
}

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401","Natural Sciences 402"]
}

/*Debug and Update*/

// Using template literals
console.log(`Student1:`)
console.log(`Hi! I'm ${student1.name}. I am ${student1.age} years old.`);
console.log(`I study the following courses: ${student1.classes}`);
console.log(`Student2:`)
console.log(`Hi! I'm ${student2.name}. I am ${student2.age} years old.`);
console.log(`I study the following courses: ${student2.classes}`);

/*
function introduce() {
	console.log("Hi! I'm " + student.name + "." + "I am " + student.age + " years old.");
	console.log("I study the following courses: " + student.class);
}
introduce(student1);
introduce(student2);
*/


/*
function getCube(num){
	console.log(Math.pow(num,3));
}
getCube(3); // result: 27
*/

let getCube = 3**3;
console.log(getCube); // result: 27


let numArr = [15,16,32,21,21,2]

/*
numArr.forEach(function(num){
	console.log(num);
})
*/

let [n1,n2,n3,n4,n5,n6] = numArr;
console.log(n1,n2,n3,n4,n5,n6);


let numPowerOf2 = (num1) => [numArr]**2;
let numSquared = numPowerOf2([numArr])
	console.log(numSquared);
 

 
// Class Constructor

class Character {
	constructor(username,role,guildname,level) {
		this.username = username;
		this.role = role;
		this.guildname = guildname;
		this.level = level;
	}
}

let character1 = new Character("Rogue Knight","fighter","sentinel",10);
let character2 = new Character("Night Stalker","assassin","scourge",15);

console.log(character1);
console.log(character2);
